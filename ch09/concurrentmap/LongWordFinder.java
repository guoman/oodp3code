import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class LongWordFinder
{
   public static Runnable findLongWords(
      String filename, int minLength, ConcurrentMap<String, Integer> map)
   {
      return () -> 
         {
            try
            {
               List<String> lines = Files.readAllLines(Paths.get(filename));
               for (String line : lines)
               {
                  String[] words = line.split("[\\PL]+");
                  for (String word : words)
                     if (word.length() >= minLength)
                        map.compute(word, (k, v) ->
                           {
                              if (v == null) return 1;
                              else return v + 1;
                           });
               }
            }
            catch (IOException ex)
            {
               // terminate task
            }
         };
   }

   public static void main(String[] args) throws Exception
   {
      String[] filenames = { 
            "../longwords/alice30.txt",
            "../longwords/war-and-peace.txt",
            "../longwords/crsto10.txt"
      };

      ConcurrentMap<String, Integer> map = new ConcurrentHashMap<>();
      
      ExecutorService service = Executors.newCachedThreadPool();
      for (String filename : filenames)
      {
         service.submit(findLongWords(filename, 15, map));
      }

      service.shutdown();
      service.awaitTermination(1, TimeUnit.HOURS);
      map.forEach((k, v) -> System.out.println(k + ": " + v + " times"));
   }
}
