import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.concurrent.*;

/**
   This program animates a sort algorithm.
*/
public class AnimationTester
{
   public static void main(String[] args)
   {
      JFrame frame = new JFrame();
      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

      ArrayComponent panel = new ArrayComponent();
      frame.add(panel, BorderLayout.CENTER);

      JButton stepButton = new JButton("Step");
      JButton runButton = new JButton("Run");

      JPanel buttons = new JPanel();
      buttons.add(stepButton);
      buttons.add(runButton);
      frame.add(buttons, BorderLayout.NORTH);
      frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
      frame.setVisible(true);

      Double[] values = new Double[VALUES_LENGTH];
      for (int i = 0; i < values.length; i++)
         values[i] = Math.random() * panel.getHeight();

      BlockingQueue<String> queue = new LinkedBlockingQueue<>();
      queue.add("Step");

      Sorter sorter = new Sorter(values, panel, queue);

      stepButton.addActionListener(event -> 
            {
               queue.clear();
               queue.add("Step");
            });

      runButton.addActionListener(event ->
            {
               queue.clear();
               queue.add("Run");
            });

      ExecutorService service = Executors.newCachedThreadPool();
      service.execute(sorter);
   }

   private static final int FRAME_WIDTH = 300;
   private static final int FRAME_HEIGHT = 300;
   private static final int VALUES_LENGTH = 30;
}
