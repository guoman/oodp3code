import java.util.concurrent.*;

public class ConcurrentGreetings
{
   public static Runnable producer(String greeting,
      BoundedQueue<String> queue, int repetitions)
   {
      return () ->
         {
            for (int i = 0; i < repetitions; i++)
               queue.add(greeting);
         };
   }

   public static Runnable consumer(
      String greeting1, String greeting2,
      BoundedQueue<String> queue, int repetitions)
   {
      return () ->
         {
            int count1 = 0;
            int count2 = 0;
            for (int i = 0; i < repetitions; i++)
            {
               String greeting = queue.remove();
               if (greeting1.equals(greeting)) count1++;
               else if (greeting2.equals(greeting)) count2++;
            }
            System.out.println(greeting1 + ": " + count1);
            System.out.println(greeting2 + ": " + count2);
         };
   }

   public static void main(String[] args)
   {
      int n = 1000;
      BoundedQueue<String> q = new BoundedQueue<>(2 * n);
      ExecutorService service = Executors.newCachedThreadPool();
      Runnable p1 = producer("Hello", q, n);
      Runnable p2 = producer("Goodbye", q, n);
      Runnable c = consumer("Hello", "Goodbye", q, 2 * n);
      service.execute(p1);
      service.execute(p2);
      service.execute(c);
      service.shutdown();
   }
}
