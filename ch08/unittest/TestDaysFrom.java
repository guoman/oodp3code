/**
   This test tests the daysFrom method.
*/
public class TestDaysFrom implements TestCase
{
   public void execute()
   {
      Day d1 = new Day(1970, 1, 1);
      Day d2 = new Day(2001, 1, 1);
      int n = d1.daysFrom(d2);
      Day d3 = d2.addDays(n);
      assertEquals(d1.getYear(), d3.getYear());      
      assertEquals(d1.getMonth(), d3.getMonth());      
      assertEquals(d1.getDate(), d3.getDate());      
   }
}
