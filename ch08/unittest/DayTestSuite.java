public class DayTestSuite extends TestSuite
{
   public TestCase[] getTestCases()
   {
      return new TestCase[]
         {
            new TestAdd(),
            new TestAddNegative(),
            new TestDaysFrom(),
            new TestGregorianBoundary()
         };
   }
}
