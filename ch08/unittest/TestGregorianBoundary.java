/**
   This test tests arithmetic around the Gregorian
   calendar change.
*/
public class TestGregorianBoundary implements TestCase
{
   public void execute()
   {
      Day d1 = new Day(1580, 1, 1);
      Day d2 = d1.addDays(MAX_DAYS);
      Day d3 = d2.addDays(-MAX_DAYS);
      assertEquals(0, d3.daysFrom(d1));      
   }

   private static final int MAX_DAYS = 10000;   
}
