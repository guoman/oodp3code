/**
   This test tests the addDays method with positive 
   parameters.
*/
public class TestAdd implements TestCase
{
   public void execute()
   {
      for (int i = 1; i <= MAX_DAYS; i = i * INCREMENT)
      {
         Day d1 = new Day(1970, 1, 1);
         Day d2 = d1.addDays(i);
         assertEquals(i, d2.daysFrom(d1));      
      }
   }
   
   private static final int MAX_DAYS = 10000;
   private static final int INCREMENT = 10;
}
