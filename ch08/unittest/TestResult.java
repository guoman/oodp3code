public class TestResult
{
   public TestResult(TestCase t)
   {
      testName = t.getClass().getName();
      passed = true;
   }

   public TestResult(TestCase t, Exception ex)
   {
      testName = t.getClass().getName();
      passed = false;
      failureDescription = ex.getMessage();
   }

   public String toString()
   {
      if (passed)
         return testName + " passed";
      else
         return testName + " failed: " + failureDescription;
   }
   
   private String testName;
   private boolean passed;
   private String failureDescription;
}
