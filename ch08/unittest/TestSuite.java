import java.util.ArrayList;
import java.util.List;

public abstract class TestSuite
{
   public abstract TestCase[] getTestCases();

   public List<TestResult> run()
   {      
      TestCase[] testCases = getTestCases();
      List<TestResult> results = new ArrayList<>();
      for (TestCase t : testCases)
      {
         try
         {
            t.execute();
            results.add(new TestResult(t));
         }
         catch (Exception ex)
         {
            results.add(new TestResult(t, ex));
         }
      }
      return results;
   }
}
