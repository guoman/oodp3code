import java.util.List;

public class TestRunner
{
   public static void main(String[] args)
      throws ReflectiveOperationException
   {
      for (String arg : args)
      {
         Class<?> testSuiteClass = Class.forName(arg);
         TestSuite suite = (TestSuite) testSuiteClass.newInstance();
         List<TestResult> results = suite.run();
         for (TestResult result : results)
            System.out.println(result);
      }
   }
}
